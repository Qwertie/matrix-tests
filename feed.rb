# This script connects to a matrix server and outputs every new message it can see in rooms
require 'json'
require 'httparty'
require 'yaml'

config = YAML.load_file('config.yml')

init_request = HTTParty.get(
    "#{config['matrix_url']}/_matrix/client/r0/sync",
    headers: {Authorization: "Bearer #{config['access_token']}"}
)

next_batch = init_request['next_batch']
while true
    req = HTTParty.get(
        "#{config['matrix_url']}/_matrix/client/r0/sync?since=#{next_batch}",
        headers: {Authorization: "Bearer #{config['access_token']}"}
    )

    puts req['rooms']['join'] if req['rooms']['join']
    next_batch = req['next_batch']
    sleep 1
end
