require 'json'
require 'httparty'
require 'yaml'

config = YAML.load_file('config.yml')

data = JSON.parse(
    HTTParty.post(
        "#{config['matrix_url']}/_matrix/client/r0/login",
        :body => {
            :type => 'm.login.password',
            :user => config['username'],
            :password => config['password']
        }.to_json,
        :headers => { 'Content-Type' => 'application/json' }
    ).body, symbolize_names: true
)

if data[:access_token]
    puts "Access token: #{data[:access_token]}"
else
    raise data.to_s
end
